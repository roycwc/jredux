package pb.redux;

public interface ReduxReducer<State, Action>{
    State reduce(State state, Action action);
}

