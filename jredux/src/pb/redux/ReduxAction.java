package pb.redux;

public class ReduxAction <Payload> {
    public Payload payload;
    public String type;

    public ReduxAction(String type, Payload payload){
        this.type = type;
        this.payload = payload;
    }

}
