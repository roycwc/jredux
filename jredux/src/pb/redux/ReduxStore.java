package pb.redux;

import java.util.ArrayList;


public class ReduxStore <State, Action> {
    public State state;
    private ReduxReducer<State, Action> reducer;
    ArrayList<Runnable> subscribers;

    ReduxStore(State initialState, ReduxReducer<State, Action> reducer){
        this.state = initialState;
        this.reducer = reducer;
        this.subscribers = new ArrayList<Runnable>();
    }

    public ReduxSubscription subscribe(Runnable runnable) {
        subscribers.add(runnable);
        return new ReduxSubscription(this, runnable);
    }

    synchronized public void dispatch(Action action) {
        State newState = reducer.reduce(state,action);
        if (newState.equals(state)) return;
        state = newState;
        for(Runnable subscriber : subscribers) subscriber.run();

    }
}
