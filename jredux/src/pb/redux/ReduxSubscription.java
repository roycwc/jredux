package pb.redux;

public class ReduxSubscription {
    private final Runnable runnable;
    private final ReduxStore store;

    ReduxSubscription(ReduxStore store, Runnable runnable) {
        this.store = store;
        this.runnable = runnable;
    }

    public void unsubscribe() {
        store.subscribers.remove(runnable);
    }
}
