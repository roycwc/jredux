package pb.redux;


public class Redux {

    public static  <State, Action> ReduxStore<State, Action> CreateStore(State initialState, ReduxReducer<State, Action> reducer){
        return new ReduxStore<State, Action>(initialState, reducer);
    }

}
