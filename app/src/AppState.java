import java.util.ArrayList;


public class AppState{
    String listName;
    ArrayList<String> names;

    AppState(AppState state) {
        names = new ArrayList<String>(state.names);
        listName = state.listName;
    }

    AppState() {
        listName = "My List";
        names = new ArrayList<String>();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof AppState)) return false;
        AppState anotherState = (AppState)obj;
        return listName.equals(anotherState.listName) &&
                names.equals(anotherState.names);
    }
}
