import pb.redux.*;

import java.util.ArrayList;

public class Main {

    private static ReduxReducer<String,ReduxAction<String>> listNameReducer = new ReduxReducer<String,ReduxAction<String>>(){

        @Override
        public String reduce(String state, ReduxAction<String> action) {
            if (action.type.equals("SET_NAME")){
                return action.payload;
            }
            return state;
        }

    };

    private static ReduxReducer<ArrayList<String>,ReduxAction<ArrayList<String>>> namesReducer = new ReduxReducer<ArrayList<String>,ReduxAction<ArrayList<String>>>(){

        @Override
        public ArrayList<String> reduce(ArrayList<String> state, ReduxAction<ArrayList<String>> action) {
            if (action.type.equals("LIST_ADD")){
                ArrayList<String> newState = new ArrayList<String>();
                newState.addAll(state);
                newState.addAll(action.payload);
                return newState;
            }
            return state;
        }
    };

    public static void main(String[] args) {

        ReduxReducer<AppState, ReduxAction> reducer = new ReduxReducer<AppState, ReduxAction>(){

            @Override
            public AppState reduce(AppState appState, ReduxAction action) {
                AppState newState = new AppState(appState);
                newState.listName = listNameReducer.reduce(appState.listName, action);
                newState.names = namesReducer.reduce(appState.names, action);
                return newState;
            }

        };

        AppState defaultState = new AppState();

        final ReduxStore<AppState, ReduxAction> store = Redux.CreateStore(defaultState, reducer);

        ReduxSubscription subscription = store.subscribe(new Runnable() {
            @Override
            public void run() {
                System.out.printf("%s has %d items \n", store.state.listName, store.state.names.size());
            }
        });

        ArrayList<String> names = new ArrayList<String>();
        names.add("roy");names.add("mary");names.add("peter");

        store.dispatch(new ReduxAction<ArrayList<String>>("LIST_ADD", names));
        store.dispatch(new ReduxAction<String>("SET_NAME", "Roy List"));


        subscription.unsubscribe();
    }

}
